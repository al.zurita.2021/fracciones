import unittest
from Fracción import Fracción,Denominador_0

class TestFracción(unittest.TestCase):

    def test_suma(self):
        n1 = Fracción(1,2)
        n2 = Fracción(1,3)

        n1.suma_me(n2)
        
        self.assertEqual(n1.numerador, 5)
        self.assertEqual(n1.denominador, 6)
    
    def test_resta(self):
        n1 = Fracción(1,2)
        n2 = Fracción(1,3)

        n1.resta_me(n2)
        
        self.assertEqual(n1.numerador, 1)
        self.assertEqual(n1.denominador, 6)
    
    def test_producto(self):

        n1 = Fracción(1,2)
        n2 = Fracción(1,3)

        n1.multiplícame(n2)
        
        self.assertEqual(n1.numerador, 1)
        self.assertEqual(n1.denominador, 6)

    def test_división(self):

        n1 = Fracción(1,2)
        n2 = Fracción(1,3)

        n1.divídeme(n2)
        
        self.assertEqual(n1.numerador, 3)
        self.assertEqual(n1.denominador, 2)
    
    def test_str(self):

        n2 = Fracción(1,4)
        self.assertEqual('1/4', n2.__str__())

    def test_Denominador_0(self):
        Fracción(1,0)
        self.assertRaises(Denominador_0)
            

if __name__ == "__main__":
    unittest.main()

'''
1. También puedes poner __str__(n2)
2. Hay que probar ¿qué pasaría si dividimos por 0?
3. Cuando hay una excepción, la levantamos y el lenguaje de programación vuelve hacia atrás hasta que encuentra un try o un exception. El try la 
captura (si no hay try, te salta el error por pantalla). Levanto la excepción donde sea capaz de detectarlo y la capturo donde la pueda arreglar.
'''