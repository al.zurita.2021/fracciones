class Denominador_0(Exception):
    def __init__(self, mensaje):
        super().__init__(mensaje)


class  Fracción:
    def __init__(self,n,d):
        self.numerador = n
        self.denominador = d
        if d == 0:
            mensaje = 'No se puede dividir por 0'
            raise Denominador_0 (mensaje)
        
    def __str__(self):
            return '{numerador}/{denominador}'.format(numerador = self.numerador, denominador = self.denominador) 
        
    def suma_me(self, n2):

        if self.denominador == n2.denominador:
            denominador_común =  n2.denominador
        else:
            denominador_común = self.denominador*n2.denominador

        numerador1 = (denominador_común/self.denominador)*self.numerador
        numerador2 = (denominador_común/n2.denominador)*self.numerador
            
        self.numerador = numerador1 + numerador2
        self.denominador = denominador_común

        return self #Devolvemos un self para realizar operaciones concatenadas 

    def resta_me(self,n2):

        if self.denominador == n2.denominador:
                denominador_común =  n2.denominador
        else:
                denominador_común = self.denominador*n2.denominador

        numerador1 = (denominador_común/self.denominador)*self.numerador
        numerador2 = (denominador_común/n2.denominador)*self.numerador
            
        self.numerador = numerador1 - numerador2
        self.denominador = denominador_común
        return self

    def multiplícame(self,n2):

        self.numerador = self.numerador*n2.numerador
        self.denominador = self.denominador*n2.denominador
        return self

    def divídeme(self,n2):

        self.numerador = self.numerador * n2.denominador
        self.denominador =  self.denominador * n2.numerador
        return self

try:
    a=1
    b=0
    fraccion = Fracción(a,b)
except Denominador_0:
    print (ZeroDivisionError)


    


